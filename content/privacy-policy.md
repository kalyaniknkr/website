---
title: "Privacy Policy of Prav App Service"
---
## Privacy Policy

Our general policy regarding your data is to store as much as needed and as little as possible. This section will try to explain what personal data we store and how we handle that.

## Contact sync

In regular intervals Prav will upload a list of phone numbers from your Android Address Book to the Prav Server and get a list of matching Jabber IDs in return. Those Jabber IDs are either other Prav users or Jabber users who have entered their phone number into the Prav Directory. The matches are kept locally in the app until you sent a message to someone. Sending a message to someone will add that particular contact to your server side contact list (roster); See section Â»Other dataÂ« below. The Prav server does not keep a copy of your entire address book.

If you enter your Jabber ID into the Prav Directory, Prav users will not be able to discover your phone number based on your Jabber ID. Only Prav users who already know your phone number can discover your Jabber ID. The Prav Directory can only be queried by registered Prav users. We employ appropriate measures to prevent Prav users from scraping the entire list.

## What we store

### Account data

Your user name (which is your phone number), a hash of your password, the date of your account creation and your last login (To automatically delete inactive accounts.)

For users of our Prav Directory service we store the combination of your Jabber ID and phone number.

### Messages

- Offline messages. If someone sends you a message while you are offline that message will be stored until you get back online.

- Archive. By default we will be keeping an archive of your messages for later retrieval by yourself. This can come in handy if you log in with a new device and want access to your message history and is also required if you want to use the OMEMO encryption with multiple devices. You can opt-out of this by setting your server-side archiving preferences with your XMPP client.

### Files

Every file you share with a contact or a conference will be uploaded and stored for later retrieval by the recipients.

### Other data

A list of your Jabber contacts (Roster, Buddylist). This list is maintained by you. This is not the Android address book that gets synchronized with our server. You decide who goes on that list and who gets deleted.

## What we don't store

Your IP address or any information that could be inferred by that address like your location.

### Your Android address book.

## How we handle your data

- If you delete your account all related information will be deleted with it. Including your files and messages.

- All backups are encrypted.

- If your Android phone goes into a standby state and loses the connection to our server we will send a wake up signal to your device over Googleâ€™s FCM. This wake up signal does not contain any personal information. Find a very detailed technical explanation [here](https://github.com/iNPUTmice/p2/blob/master/README.md).

- We hand your Jabber ID or Prav username out to any Prav user who knows your full phone number.

- Even though we will never look at the contents of your messages or files nor process them in an automated fashion we strongly advise you to use the OMEMO encryption whenever possible.

Credits: This privacy policy is based on <a href="https://quicksy.im/#privacy">Quicksy Privacy Policy</a>.

<b>Note: SMS providers will be updated when we start using an SMS provider.</b>

---
title: "Become a Member"
---
### Introduction

In order to register as a Multi State Cooperative Society in India, we need 50 members each from two states of India. We have completed 50 members milestone from Kerala and we need 50 members from one more state. To be a member, you need to commit to buying at least one share, which is currently set to worth 1,000 INR. Scroll down and you will find a form. Please fill that form to pledge membership of the cooperative. 

By registering as a member of the Prav Multi State Cooperative Society, you can help us with your experience, knowledge and the amount that you give for buying shares will help us in funding for the app and running the service. Plus, your membership will help us cover the legal requirements for becoming a cooperative society.

<b>In case, we fail to reach 50 members from a second state by June 15 2023, we plan to register as a cooperative in Kerala and whenever we meet the necessary requirements in other states, we plan to register as a multi state cooperative society.</b>
 
This is a graph showing the progress from first two states:

<img src="/images/members-graph.png">

Below table shows the number of people from each state who committed to be a member of Prav Cooperative Society:

*Last Updated: 26 June 2023*

<table>
  <tr>
    <th>State</th>
    <th>Number of members registered</th>
  </tr>
  <tr>
    <td>Kerala</td>
    <td>60</td>
  </tr>
  <tr>
    <td>Maharashtra</td>
    <td>37</td>
  </tr>
  <tr>
    <td>Tamil Nadu</td>
    <td>11</td>
  </tr>
  <tr>
    <td>Karnataka</td>
    <td>10</td>
  </tr>
  <tr>
    <td>Uttar Pradesh</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Bihar</td>
    <td>2</td>
  </tr>
  <tr>
    <td>Madhya Pradesh</td>
    <td>2</td>
  </tr>
  <tr>
    <td>West Bengal</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Himachal Pradesh</td>
    <td>1</td>
    <td></td>
  </tr>
  <tr>
    <td>Odisha</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Rajasthan</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Andhra Pradesh</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Jammu and Kashmir</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Telangana</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Haryana</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Chandigarh</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Goa</td>
    <td>1</td>
  </tr>
</table>

The table below shows number of members from districts of Kerala. The data below is incomplete as we are yet to know other members' districts. This will be useful for plan B-- registering in Kerala if we fail to get 50 members from any other Indian state. We need 25 people from one district of Kerala for the same.

<table>
  <tr>
    <th>District</th>
    <th>Number of members registered</th>
  </tr>
  <tr>
    <td>Thrissur</td>
    <td>14</td>
  </tr>
  <tr>
    <td>Kozhikode</td>
    <td>9</td>
  </tr>
  <tr>
    <td>Palakkad</td>
    <td>8</td>
  </tr>
  <tr>
    <td>Malappuram</td>
    <td>4</td>
  </tr>
  <tr>
    <td>Ernakulam</td>
    <td>3</td>
  </tr>
  <tr>
    <td>Kollam</td>
    <td>2</td>
  </tr>
  <tr>
    <td>Kottayam</td>
    <td>2</td>
  </tr>
  <tr>
    <td>Thiruvananthapuram</td>
    <td>2</td>
  </tr>
  <tr>
    <td>Alappuzha</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Idukki</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Kannur</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Kasaragod</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Wayanad</td>
    <td>1</td>
  </tr>
</table>

Please fill the form below to register as a member.

<b>Warning: The form might not work in private/incognito window of your browser. Some browsers might have issues in loading the form. If the form does not load, please disable any ad blockers or extensions enabled in your browser and refresh the page. If you are not able to fill the form with multiple browsers, please send us an email to prav at fsci dot in.</b>

<div id="kobo-form" style="height: 100vh">
    <iframe frameborder="2" src="https://ee.kobotoolbox.org/single/8c1323b140de267cb43c56c487c93722?returnUrl=https://prav.app/thanks.html" width="100%" height="100%" style="margin: auto;" ></iframe>
</div>

<br>

### Why Become A Member?

Look at the image below:

<figure>
<img src="/images/19-jan-2022_World_map_of_share_of_the_population_fully_vaccinated_against_COVID-19_by_country.png" width="500" height="300">
<figcaption><a href="https://upload.wikimedia.org/wikipedia/commons/e/ee/World_map_of_share_of_the_population_fully_vaccinated_against_COVID-19_by_country.png">Source: Wikipedia's version of the file uploaded on 3-December-2021</a> </figcaption>
</figure>

<br>
What does the above image illustrate?

The image goes to show how unequal the vaccine distribution has been across the countries.

[Quoting the official data](https://web.archive.org/web/20211204163451/https://ourworldindata.org/covid-vaccinations) on Covid vaccines,
> 54.9% of the world population has received at least one dose of a COVID-19 vaccine.
> Only 6.2% of people in low-income countries have received at least one dose.

(on 4th December 2021)

Why is it so?

Is there a scarcity of vaccine units, or is the vaccine being developed at a slow rate?

No!

In fact, the vaccine production is accelerating, but [the wealthy countries are restricting the access intentionally](https://www.theguardian.com/commentisfree/2021/sep/09/west-vaccine-doses-covid-production). Also, developed countries have preordered more number of vaccines than they require and there are only a few companies developing these vaccines, which means that people in low-income developing countries [may not receive vaccinations from these manufacturers until 2023 or 2024](https://www.eiu.com/n/85-poor-countries-will-not-have-access-to-coronavirus-vaccines).

The patent on Covid 19 vaccine limited the knowledge of making vaccine to a few companies. And so many developing countries do not have access to the vaccine due to their dependence on rich countries for the vaccine. The developing countries do not enjoy the same resources to put into research of making vaccine, nor they have equal access to knowledge and opportunities as rich countries have. Had the formula being known and not kept secret, the local manufacturers in all the countries could make the vaccine, providing access to many more people. In this system, the few companies having patent on the vaccine are making all the decisions and others at
their mercy.

Currently, technology is given to us in a similar form, like a blackbox-- either accept it as it is or don't use it at all. This makes users helples
s and dependent on the creators of the technology. Only a few countries will have resources and others will be dependent on them for technology.
This type of distribution of technology makes us dependent on a few companies for all our digital needs.

As an example, most people use WhatsApp for their communications. It is controlled by Facebook. From its source code to all its policies, everything is in the hands of Facebook. Users only have two choices- either use it or leave it. Leaving the platform means you lose all your contacts. If you switch to another messaging app, you need to convince every contact to use the new messaging app. The whole communications of the world get dependent on a single company. And they exploit you by collecting your data by spying on you.

A good news is that now we have a patent-free vaccine Corbevax. And this patent-free vaccine is similar to what we aim to do in the field of technology.

We want to change how technology works. Free Software empowers the users. Users are not dependent on a company or the developer of the program. In the era where surveillance and data collection is the norm, we want to give users privacy and control of the software running in their devices.

Some of our members are already involved in bringing this change by running privacy-respecting services for around 8 years now. You can see a list of these services [here](https://fsci.in/#poddery). Those services are run on voluntary basis by people in their free time.

The business model we have decided is to provide a messaging service to users on subscription basis. Users will pay a subscription fees per month. Keep in mind that no service is free-of-cost and usually they are funded by exploiting the users, by putting them under surveillance. Others like poddery are funded by donations. In these business models, either the users get exploited or people work in their free time without getting paid. We want to pay the people for their work and have an ethical business model without exploiting the users. The best way is to charge users a subscription fee.

We want to base our business on values like empowering users and users getting control of the technology they use rather than being at the whim of some corporates. We also do not want the company to compromise the values. Therefore, we want to encourage more participation by common people so tha
t it acts like a democracy. We will give common people decision making powers so that the company decisions are not dictatorial in nature. So, we want to register it like a cooperative. We could register it as a local state cooperative, but we want involvement from the whole country. Therefore, we are thinking on making it a Multi-State Cooperative Society.

[Quoting Wikipedia](https://en.wikipedia.org/wiki/Cooperative) on Cooperative, "Cooperative businesses are typically more productive and economically resilient than many other forms of enterprise, with twice the number of co
-operatives (80%) surviving their first five years compared with other business ownership models (41%) according to data from United Kingdom. The
largest worker owned cooperative in the world, the Mondragon Corporation (founded by Catholic priest José María Arizmendiarrieta), has been in cont
inuous operation since 1956."

We have estimated that we need around Rs 20 Lakh investment for running the service for three years, including paying employees, advertising and marketing etc.

If you have any queries, please get in touch by sending us an email to prav at fsci dot in.

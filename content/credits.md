---
title: "Credits"
date: 2023-01-21T19:21:20+05:30
draft: false
author: false 
---
## Contributors

- Pirate Praveen: Helped in outreach and supervising the project and guided Ravi for deploying Quicksy server.

- Chagai for building the apk of prav app.

- Ravi Dwivedi helped in maintaining prav website, social handle, setting up prav server and project outreach activities. 

- Akshay S Dinesh helped by troubleshooting from time to time and adapting quicksy server.

- Badri and Suman did the initial ejabberd setup

- Sajith and Badri helped in domain and hosting.

- Raghukamath designed the prav logo.

- Mani built the prav beta release apk.

- [VGLUG](https://vglug.org) hosted prav app beta release party and translated the poster in Tamil.

- Badri created the beta release party poster and the countdown for beta release on the website.

## Artwork

See [artwork page](/artwork)

## Software we forked

- [Quicksy](https://quicksy.im) client and Quicksy server.

## Software we use

- [Debian](https://debian.org)

- ejabberd

- [hugo](https://gohugo.io/)

- Hugo Serif Theme by www.zerostatic.io for the website

- [Penpot](https://penpot.app) for designing posters.

## Services we use

- [FSCI mailing lists](https://lists.fsci.in)

- [codeberg](https://codeberg.org)

- [kobotoolbox](https://kobotoolbox.org)

- [Cryptpad](https://cryptpad.fr)

- [codema](https://codema.in)

---
title: "FAQ"
---

## General

### What is Prav?

Prav is an end-to-end encrypted by default, intuitive & easy-to-use free software instant messenger. Prav App is convenient without vendor lock-in unlike most common messaging apps.

### Why the name 'Prav'?

'Prav' means pigeon in Malayalam, the emblem of universal peace.

The name was chosen by our community poll. The screenshot below shows all the other names proposed and how much votes they got.

<img src="/images/prav-poll.png">

### What is vendor lock-in?

Most common messaging apps that are convenient(like WhatsApp, Telegram, Signal) allow you to talk to people who use the same app only, limiting users' choice. This is known as vendor lock-in.

### How does prav avoid vendor lock-in?

Prav allows you to talk to Quicksy users, users of any other [apps that support XMPP protocol](https://xmpp.org/software/clients/), a free standard for messaging, and users of any [XMPP service](https://providers.xmpp.net/).

### Where can I download the app?

If you are interested in trying out the private beta app, please fill the form [here](/beta-release-party). We are planning to release a public beta version soon.

### We already have many messenger apps available, what is the need for another one?

There are many reasons which make prav unique.

- Popular messenger apps like WhatsApp are proprietary software, which is controlled by the developer and that often makes [proprietary software a malware](https://gnu.org/malware), which is then [used for surveillance of users](https://www.gnu.org/proprietary/proprietary-surveillance.html). We want to give users control of the software. Therefore, Prav app will be released as [Free/Swatantra Software](https://www.gnu.org/philosophy/free-software-even-more-important.html), which gives users freedom and control over the software. Here 'free' refers to 'freedom' and not price. Providing source code of the app promotes a culture of transparency, and the code can be audited independently. On the other hand, any trust on proprietary software is a blind trust.

- Another major problem is centralization: WhatsApp, Telegram and Signal are all centralized services. They are controlled by one entity. We don't think that the communcations of the whole world relying on a single company is sustainable. Facebook outage [is a reminder of this](https://mashable.com/article/facebook-outage-small-business-impact). We think more and more instant messenger services coming up and interoperable(user of service A can talk to user of service B) is better as users will have more choices of service providers, similar to email services and telecom operators. This will give users real control over the messaging software and services they use. 

- Prav combines good ideas from [Quicksy](https://quicksy.im) and [Snikket](https://snikket.org). Quicksy is available only for Android, Snikket has focus on users across platforms but it is targeted to a limited set of people who can afford to self-host.

- For most projects, core developers decide what features gets implemented. In Prav, the community decides which features are a priority and fund developers to implement it. Prav is an experiment to democratize the right to modify. Currently it is mostly applied as individual developers or collection of developers. We involve users in a more crucial role. Big organizations use this right to modify already but not many projects give this power to non-developers. Within xmpp too, most projects are driven by developers but Prav is driven by users.

### What is interoperability?

Priya gets a funny meme sent to her on Instagram. She wants to forward it to Faizal. But Faizal doesn’t have an Instagram account, although they do have a WhatsApp account. Priya downloads the meme to her phone and sends it to Faizal on WhatsApp. If WhatsApp and Instagram were interoperable she could have sent it to them directly from Instagram. Also, now she has to use two apps and depending on the diversity of her contacts, she is forced to use those apps, even if she don't like some of them.

### I use every app for free? What are you talking about 'Free Software'?

When we use the term 'Free Software', we mean *"Free" as in "Freedom", not "Free of cost"*. Free Software means users have freedom to use, study, modify, share and share the modified versions of the software. Free Software is a matter of liberty and not price. Some examples of Free Software are: Firefox browser, VLC media player, Ubuntu etc. Conversations app is a Free Software, which user needs to pay to install from the Google Play Store.

### Is Prav a paid service, or free of cost?

Prav will be a paid service, but this demands some elaboration. Users can install Prav for no price from app stores but account creation will cost an amount. We don't intend to use your private conversations, nor your metadata to make money. If you want to use free of cost services, you will find many XMPP services that you can use without any fee, and you can still talk to Prav users. One example is Quicksy app for Android.

### Why is Prav a paid service?

It is because we care for your privacy. Popular messenger apps might be free-of-cost in terms of money, but usually they are funded in hidden ways, like selling your data. This violates your right to privacy. Another way the services get funded is by voluntary user donations. Instead, we choose to charge a nominal subscription fee for providing the service. We won't collect any data in the first place, as user rights are deeply ingrained in our [Social Contract](/about#social-contract).

### How much will the account cost?

Currently, the mark has been set for ₹200 per user for a period of 3 years. That means users will have to renew their subscriptions every 3 years. However, this is a tentative decision and will depend upon how many users subscribe to our service.

### Wait! What will I do with Prav app if I don't want to pay for the account? I see this is a trick of yours to bind us into your service with your app.

Dear user, please note that this is not the case. As per our [**Social Contract**,](/about#social-contract/) our app is designed to allow you choose the XMPP service of your choice. We, the ones behind Prav are committed to make people know that instant messengers don't have to lock-in their users. Likewise, and conversely, Prav accounts can be used on other apps that support XMPP. 

### What can I do with Prav? What are the features? 

Prav will have all the features that you expect from an instant messenger. It has file sharing, audio/video calls, contact discovery, and of course, messages will be end-to-end encrypted by default!

### So, Prav runs on XMPP. What is this indeed? 

[XMPP](https://en.wikipedia.org/wiki/XMPP) is an open protocol for exchange of information in real-time, and to be simple, it does so by the exchange of data in XML formats. As such, you can rest assured that all your data, and your metadata, is in such a protocol which is open to verification, modification and extension. Add on top of that another open encryption protocol: [OMEMO](https://conversations.im/omemo/) and you might have understood by now why Prav is a very secure and privacy-respecting messenger!

### Can I talk to my WhatsApp contacts using Prav app? 

Even though we would like to be able to exchange messages with WhatsApp, Telegram or Signal, they don't want to allow users of an independent messaging provider to talk to their users.

### Why not just use [Matrix](https://matrix.org)?

Philosophically both matrix and XMPP are equivalent due to federation and interoperability. Due to some design choices, we prefer xmpp over matrix (semi-anonymous public groups as we want to use phone number as id, lighter groups due to groups on single servers though that loses some redundancy, etc).


Matrix costs more in terms of system resources and effort to manage (we have experience of running poddery.com and diasp.in which has both matrix and xmpp). These extra costs do offer some benefits like redundancy of messages, but we feel that may not be required for a general messaging system. Though organizations that can afford to pay more may still find Matrix better.

Messages are stored on all participating servers in matrix, and they are stored forever by default, in contrast, xmpp groups are hosted on a single server and by default messages are deleted after some time (this can be configured by the admin). This also means matrix servers have to continuously merge the state and history across all participating servers (this can be thought of like a git repo being forked and merged all the time) and this takes a lot of cpu and ram.

Matrix do have better client apps compared to xmpp right now, but we feel this can be improved over time and the rough edges to xmpp clients can be fixed, especially since there is a lot of people coming back to xmpp. We also hope to invest in fixing some of these missing features in xmpp.

## Decision Making

### Who can join the project? 

We are open to anyone interested in joining the project, the only requirement being abiding to our [Code of Conduct](/coc). Though, we will soon register as a cooperative society and it will have other requirements to join which will be specified in its bylaws.

### How do you make decisions?

We try to make consensus, but in case we can't resolve differences, we will go with the majority. All decisions have to be within our [Social Contract](/about/#social-contract), which can be changed only with consensus.

### How do you prioritize feature development? 

Anyone can propose a feature they like. These are listed in a poll periodically. Most voted features are prioritized.

## I want to join the project, how do I join? 

You can contact us using any of the options listed on the [website](#contact). Check the footer.

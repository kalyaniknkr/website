---
title: "Inviting Developers for Prav Public Beta Release"
date: 2023-06-14T18:52:55+05:30
draft: false
author: false
---
Prav app is currently in private beta and we are planning to release a public beta soon for everyone to try out the service. We are looking for a Java developer with knowledge of Android app development. Please send us your proposals along with the amount of money required for you to work on the project.  

## Features to implement

We need these features to be implemented in the public beta version of the prav app:

1. The app should support message retractions.

2. Users should be able to sign up with a custom username instead of taking phone number as username. This will need change in both app and server side.

3. Publish the app in [F-Droid](https://f-droid.org) store.

4. Enable sign up by SMS OTP (configure SMS gateway in server).

## Suggested order of steps

1. Start with monocles chat, i.e., fork this [repository](https://codeberg.org/Arne/monocles_chat/) - this will bring retractions support to the app.

2. Apply [prav changes on top](https://codeberg.org/prav/prav) as a gradle build flavor (instead of directly changing the code).

3. Build quicksy flavor. [Quicksy](https://quicksy.im) is built from [Conversations](https://conversations.im) as a gradle build flavor. So in theory, it should be possible to build Quicksy flavor from Monocles Chat as well. 

4. We have bought Twilio credits but we need to use their v2 api and test SMS gateway integration. SMS gateway integration is already implemented in quicksy server with twilio api v1 which is not available to new customers, so we need to migrate to api v2 (this will be useful for Quicksy in the future so we can submit this as a pull request). For details, check out [their docs](https://www.twilio.com/docs/verify/migrating-1x-2x).

5. Modify Prav server to allow creating custom username (this will add the xmpp id and phone number mapping in the directory instead of using phone number as username)

6. Modify login screen to add an option to choose a custom username.

	1. Add a warning popup about using phone number as username and offer an option to choose custom username

		a. In direct chats and private groups, people will see your phone number and in public groups, admins will see your phone number. If you want to chat with someone new, you will have to share your phone number.

		b. If you use xmpp to irc bridges or join rooms which use such bridges, your phone number will be public.

	2. Use the newly created API to create account with custom username and link to phone number in the directory.

## Contact us to apply

If you are interested, please use the contact details in the [footer](#contact) of this page to reach us. The source code of Prav app is [here](https://codeberg.org/prav/prav). We are tracking the progress of public beta release [here](https://codeberg.org/prav/IssueTracker/milestone/4008).

---
title: 'Homepage'
meta_title: 'Prav App'
description: "Prav Messaging App- Reclaiming choice of service providers"
intro_image: "/images/prav-beta-launch-1.jpg"
intro_image_absolute: false
intro_image_hide_on_mobile: false
---
# Prav Messaging App- Reclaiming choice of service providers

Prav App is convenient without [vendor lock-in](https://en.wikipedia.org/wiki/Vendor_lock-in). We are currently in private beta phase and planning for a public beta release of prav app.

<style>
.button
{
background-color: #ff1493;
border:none;
}
<div>
</style>
<a href="/about"><button class="button">Learn More</button></a>

<a href="/beta-release-party"><button class="button">Request Beta Access</button></a>

Do you have experience with Android app development? We are inviting Android developers for creating prav public beta app as paid work.

<style>
.button
{
background-color: #ff1493;
}
<div>
</style>
<a href="/blog/inviting-developers-for-prav-public-beta/"><button class="button">Learn More</button></a>

<br>
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://videos.fsci.in/videos/embed/7268cae2-218c-4692-a7ac-84102799746c" frameborder="0" allowfullscreen></iframe>

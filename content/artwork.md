---
title: "Prav Artwork"
date: 2023-01-22T00:45:08+05:30
draft: false
author: false 
---
<figure>
<img src="/images/logo.png">
<figcaption>Prav logo. Credits: <a href="https://raghukamath.com/">Raghukamath</a>. 
</figcaption>
</figure>

<figure>
<img src="/images/prav-private-beta.png" width="300px">
<figcaption>Poster for prav beta release party. 
Credits: <a href="https://fosstodon.org/@badrihippo">Badri Sunderarajan</a>
</figcaption>
</figure>

<figure>
<img src="/images/prav-private-beta-mobile-1.png" width="300px">
<figcaption>Poster for prav beta release party for smaller screens. 
Credits: <a href="https://fosstodon.org/@badrihippo">Badri Sunderarajan</a>
</figcaption>
</figure>
<figure>
<img src="/images/prav-private-beta-mobile-2.png" width="300px">
<figcaption>Poster for prav beta release party for smaller screens. 
Credits: <a href="https://fosstodon.org/@badrihippo">Badri Sunderarajan</a>
</figcaption>
</figure>
<br>
<figure>
<img src="/images/prav-private-beta-ta.png" width="300px">
<figcaption>Tamil translation of the poster for prav beta release party.
Credits: <a href="https://fosstodon.org/@badrihippo">Badri Sunderarajan</a>
Translation Credits: <a href="https://vglug.org">VGLUG</a>
</figcaption>
</figure>

<br>
<figure>
<img src="/images/prav-private-beta-mobile-1-ta.png" width="300px">
<figcaption>Tamil translation of the mobile poster for prav beta release party.
Credits: <a href="https://fosstodon.org/@badrihippo">Badri Sunderarajan</a>
Translation Credits: <a href="https://vglug.org">VGLUG</a>
</figcaption>
</figure>
<br>
<figure>
<img src="/images/prav-private-beta-mobile-2-ta.png" width="300px">
<figcaption>Tamil translation of the mobile poster for prav beta release party.
Credits: <a href="https://fosstodon.org/@badrihippo">Badri Sunderarajan</a>
Translation Credits: <a href="https://vglug.org">VGLUG</a>
</figcaption>
</figure>

<br>
<figure>
<img src="/images/prav-private-beta-hi.png" width="300px">
<figcaption>Hindi translation of the poster for prav beta release party.
Credits: <a href="https://fosstodon.org/@badrihippo">Badri Sunderarajan</a>
Translation Credits: Vijay Kumar.
</figcaption>
</figure>

<br>
<figure>
<img src="/images/prav-private-beta-mobile-1-hi.jpg" width="300px">
<figcaption>Hindi translated mobile poster for prav private beta.
Credits: <a href="https://fosstodon.org/@badrihippo">Badri Sunderarajan</a>
Translation Credits: Vijay Kumar.
</figcaption>
</figure>
<br>
<figure>
<img src="/images/prav-private-beta-mobile-2-hi.jpg" width="300px">
<figcaption>Hindi translated mobile poster for prav private beta.
Credits: <a href="https://fosstodon.org/@badrihippo">Badri Sunderarajan</a>
Translation Credits: Vijay Kumar.
</figcaption>
</figure>

For sources of these artwork, please visit [this page](https://codeberg.org/prav/prav-assets).

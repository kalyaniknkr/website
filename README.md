# Website

The official website of Prav App. The website is at https://prav.app. 

### License

The source code of Prav website is released under GNU GPL v3. You can check out the license [here](https://codeberg.org/prav/website/src/branch/main/LICENSE).
